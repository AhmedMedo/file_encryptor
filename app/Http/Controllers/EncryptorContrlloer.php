<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Encryption\FileEncryptor;
use App\FileEncryptor as FileEnc;
class EncryptorContrlloer extends Controller
{
    //

    protected $key ='key';

    public function index()
    {
        $files = FileEnc::all();
        return view('dashboard', compact('files'));
    }

    public function encrypt(Request $request)
    {
    	//GEt File Content and main data
    	$file_name = $request->file('file')->getClientOriginalName();
    	$extension = $request->file('file')->extension();
    	$size = $request->file('file')->getSize();


    	$content = file_get_contents($request->file('file'));

    	//Save Main File to main folder
    	$enc = new FileEncryptor($this->key);
    	$encrypted_data = $enc->encrypt($content);

    	\File::put('encrypt_files/'.$request->file_name.'.'.$extension,$encrypted_data);

    	//Save Data
    	$file = new FileEnc();
    	$file->name = $file_name ;
    	$file->size = $size ; 
    	$file->extension = $extension;
    	$file->main_path =  $request->file('file')->move('main',time().'.'.$request->file('file')->extension());
    	$file->encrypt_path	= 'encrypt_files/'.$request->file_name.'.'.$extension;
    	$file->save();

    	return redirect()->back();

    }


    public function decrypt(Request $request)
    {
        $file = FileEnc::find($request->file_id);
        $content = \File::get($file->encrypt_path);
         $dec = new FileEncryptor($this->key);
         $decrypted_data = $dec->decrypt($content);
         \File::put('decrypt_files/'.$file->name.'.'.$file->extension,$decrypted_data);
         
        return redirect()->back();

    }
}
