@extends('layouts.app')
@section("header")
<h1>FileUploader
        <small>Uplaod and encrypt files here</small>
      </h1>
@endsection

@section("content")
<div class="row">
    <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Upload File</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" action="{{route('encrypt')}}" enctype='multipart/form-data'>
              @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">File Name</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" name="file_name" placeholder="File Name">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" name="file" id="exampleInputFile">
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Encrypt</button>
              </div>
            </form>
          </div>
    </div>
          <div class="col-md-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">View All Files</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>ID</th>
                    <th>file name</th>
                    <th>size</th>
                    <th>extension</th>
                    <th>actions</th>
                  </tr>
                  </thead>
                  <tbody>

                    @foreach($files as $file)
                      <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$file->name}}</td>
                        <td>{{$file->size}}</td>
                        <td>{{$file->extension}}</td>
                        <td>
                          <form method="POST" action="{{route('decrypt')}}">
                            @csrf
                            <input type="hidden" name="file_id" value="{{$file->id}}"> 
                             <button type="submit" class="btn btn-danger">Dycrypt</button>
                          </form>
                        </td>
                      </tr>

                    @endforeach

                  </tbody>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
          <!-- /.box -->
          </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
        $('#example1').DataTable();
</script>

@endpush